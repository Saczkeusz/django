from django.db import models

# Create your models here.
class Category(models.Model):
    category = models.CharField(max_length=32)

class Task(models.Model):
    title = models.CharField(max_length=24)
    description = models.CharField(max_length=200)
    isDone = models.BooleanField()
    category = models.ForeignKey(Category, models.CASCADE)


