from django.db import models

class Question(models.Model):
    qText = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')
    qType = models.BooleanField(default=False) #false - closed task, true - open task

class Choice(models.Model):
    question = models.ForeignKey(Question, models.CASCADE)
    choice_text = models.CharField(max_length=20)
    votes = models.IntegerField(0)

class Answer(models.Model):
    question = models.ForeignKey(Question, models.CASCADE)
    answer = models.CharField(max_length=20)

