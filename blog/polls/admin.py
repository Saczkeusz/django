from django.contrib import admin
from .models import Question,Choice,Answer

# Register your models here.
@admin.register(Question)                       
class QuestionAdmin(admin.ModelAdmin):          
   list_display = ('qText', 'pub_date', 'qType')
   list_filter = ('pub_date', 'qType') 
   search_fields = ('question_text',)  

@admin.register(Choice)                               
class ChoiceAdmin(admin.ModelAdmin):                  
   list_display = ('question', 'choice_text', 'votes')
   list_filter = ('question',)               
   search_fields = ('choice_text',)  
   
@admin.register(Answer)                      
class AnswerAdmin(admin.ModelAdmin):         
   list_display = ('question', 'answer')     
   list_filter = ('question',)                       
   search_fields = ('answer',)  